// ---------------------------------------1---------------------------
const AllParagraphs = document.getElementsByTagName('p');

console.log(allParagraphs);

for (let i = 0; i < allParagraphs.length; i++) {
    allParagraphs[i].style.backgroundColor = " #ff0000";
}

// --------------------------------------2-------------------------------
const ElementId = document.getElementById('optionsList');
// found id:
console.log(ElementId);
// found father:
console.log(ElementId.parentElement);
// checked Node.hasChildNodes() and found names and type
for (let i = 0; i < ElementId.childNodes.length; i++ ) {
    console.log(ElementId.childNodes[i]);
    console.log(typeof ElementId.childNodes[i]);
}

// --------------------------------3---------------------------------
const contentParagraph= document.getElementById('testParagraph');
contentParagraph.innerHTML = "This is a paragraph";

// console.log(contentParagraph);

// --------------------------------------4---------------------------------
const parentElements = document.getElementsByClassName('main-header');
console.log(parentElements[0].children);

for( let i = 0; i < parentElements[0].children.length; i++) {
    parentElements[0].children[i].classList.add('nav-item');
    console.log(parentElements[0].children[i])
}

// ---------------------------------------6---------------------------
const elToRemove = document.getElementsByClassName('section-title');
console.log(elToRemove);

for (let index in elToRemove) {
    if (typeof elToRemove.item(index) === 'object' && elToRemove.item(index) !== null) {
        console.dir(elToRemove.item(index))
        elToRemove.item(index).classList.remove('section-title');
    }
}

//-------------------------------------------------------------------------------
/*
1. Document Object Model (DOM) -модель HTML DOM побудована як дерево об'єктів і, коли веб-сторінка завантажується, браузер створює об'єктну модель документа сторінки.
2. властивість innerHTML витягує весь контент разом із тегами із зазначеного елемента на HTML, властивість textContent витягує весь контент без тегів із зазначеного елемента на HTML сторінці.
3. Щоб звернутися до елемента на сторінці потрібно застосувати методи пошуку: document.getElementById(id), elem.getElementsByTagName(tag), elem.getElementsByClassName(className),document.getElementsByName(name), querySelectorAll, elem.querySelector(css).
 */